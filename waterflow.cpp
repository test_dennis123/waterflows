#include "waterflow.h"
#include "Arduino.h"
volatile static int flow_frequencys;
static void flow()                  // Interruot function
	{ 
	   flow_frequencys++;
	} 
waterflow::waterflow()                  // Interruot function
	{ 
	
	} 

waterflow::setup_Nano(int attachinterrupt_pin)                  // Interruot function
	{ 
		pinMode(attachinterrupt_pin, INPUT);
		
		sei();                            // Enable interrupts  
		currentTime = millis();
		cloopTime = currentTime;
		//attachInterrupt(attachinterrupt_pin, flow, RISING);
		attachInterrupt(digitalPinToInterrupt(attachinterrupt_pin), flow, RISING);
		
	} 
float waterflow::GetFlow()                  // Interruot function
	{ 
		interrupts();
		delay(1000);  //這個參數千萬不可動!!! 
		noInterrupts();
		currentTime = millis();
		// Every second, calculate and print litres/hour
		if(currentTime >= (cloopTime + 1000))
				{     
						cloopTime = currentTime;              // Updates cloopTime
						// Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min. (Results in +/- 3% range)
						l_hour = (flow_frequencys * 60 / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/hour 
						L_min=(flow_frequencys / 7.5); // (Pulse frequency ) / 7.5Q = flow rate in L/min 
						flow_frequencys = 0;                   // Reset Counter
						//Serial.print(l_hour, DEC);            // Print litres/hour
						//Serial.println(" L/hour-------------------------------------------------");
						
						            // Print litres/hour
						interrupts();
						
						//if ((float)l_hour==3072) return 0; 
						flow_rate=L_min;
						Serial.print(L_min, DEC);
						Serial.println(" L_min");
						return L_min;
				}
	} 
