/*
 * waterflow.h
Description：該類別是用於取得水量計資料
function name：setup_Nano()
        Description：用於在setup裡面初始化感測器的設定，設定要讀取編碼器的數位腳位，在呼叫GetFlow()之前要先呼叫此函數
        Syntax： waterflow_A.setup_Nano(val)
        Parameters：val：要用於計數編碼器資料的腳位
        Returns：non
function：GetFlow()
        Description：取得感測器當下量測到的水量
        Syntax： waterflow_A.GetFlow()
        Parameters：non
        Returns：水量資料，單位是L/min 資料型態：int
*/

#include "waterflow.h"
waterflow waterflow_A;
void setup() 
  {
    waterflow_A.setup_Nano(2);
  }

void loop() 
  {
    int flow = waterflow_A.GetFlow();
  }
